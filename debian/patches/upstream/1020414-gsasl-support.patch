From 5d8a5fe3f28d1efc583199a840975616b9fed2e2 Mon Sep 17 00:00:00 2001
From: Richard Russon <rich@flatcap.org>
Date: Fri, 16 Sep 2022 17:24:39 +0100
Subject: [PATCH] Add GNU SASL support for authentication

add support for the GNU SASL library, using configure option --gsasl

Add multiline response support for SMTP authentication (which is
probably not actually needed).  Also add arbitrary line length for the
SASL server responses (the RFCs note that for SASL, the protocol line
lengths don't apply).

Upstream-commit: https://gitlab.com/muttmua/mutt/commit/68caf9140c8217ecf6c848460c4b4d27996b2922

--- a/Makefile.autosetup
+++ b/Makefile.autosetup
@@ -269,7 +269,10 @@
 LIBCONN=	libconn.a
 LIBCONNOBJS=	conn/config.o conn/connaccount.o conn/getdomain.o conn/raw.o \
 		conn/sasl_plain.o conn/socket.o conn/tunnel.o
-@if HAVE_SASL
+@if USE_SASL_GNU
+LIBCONNOBJS+=	conn/gsasl.o
+@endif
+@if USE_SASL_CYRUS
 LIBCONNOBJS+=	conn/sasl.o
 @endif
 @if USE_SSL
@@ -450,9 +453,13 @@
 @if USE_GSS
 LIBIMAPOBJS+=	imap/auth_gss.o
 @endif
-@if HAVE_SASL
+@if USE_SASL_CYRUS
 LIBIMAPOBJS+=	imap/auth_sasl.o
-@else
+@endif
+@if USE_SASL_GNU
+LIBIMAPOBJS+=	imap/auth_gsasl.o
+@endif
+@if !HAVE_SASL
 LIBIMAPOBJS+=	imap/auth_anon.o imap/auth_cram.o
 @endif
 CLEANFILES+=	$(LIBIMAP) $(LIBIMAPOBJS)
--- a/auto.def
+++ b/auto.def
@@ -57,6 +57,8 @@
   gss=0                     => "Use GSSAPI authentication for IMAP"
   with-gss:path             => "Location of GSSAPI library"
   # SASL (IMAP and POP auth)
+  gsasl=0                   => "Use the GNU SASL network security library"
+  with-gsasl:path           => "Location of the GNU SASL network security library"
   sasl=0                    => "Use the SASL network security library"
   with-sasl:path            => "Location of the SASL network security library"
   # AutoCrypt
@@ -154,7 +156,7 @@
   foreach opt {
     asan autocrypt bdb coverage debug-backtrace debug-color debug-email
     debug-graphviz debug-notify debug-parse-test debug-queue debug-window doc
-    everything fmemopen full-doc fuzzing gdbm gnutls gpgme gss homespool idn
+    everything fmemopen full-doc fuzzing gdbm gnutls gpgme gsasl gss homespool idn
     idn2 include-path-in-cflags inotify kyotocabinet lmdb locales-fix lua lz4
     mixmaster nls notmuch pcre2 pgp pkgconf qdbm rocksdb sasl smime sqlite ssl
     testing tdb tokyocabinet zlib zstd
@@ -166,9 +168,9 @@
   # relative --enable-opt to true. This allows "--with-opt=/usr" to be used as
   # a shortcut for "--opt --with-opt=/usr".
   foreach opt {
-    bdb gdbm gnutls gpgme gss homespool idn idn2 kyotocabinet lmdb lua lz4 
-    mixmaster nls notmuch pcre2 qdbm rocksdb sasl sqlite ssl tdb tokyocabinet 
-    zlib zstd
+    bdb gdbm gnutls gpgme gsasl gss homespool idn idn2 kyotocabinet lmdb lua
+    lz4 mixmaster nls notmuch pcre2 qdbm rocksdb sasl sqlite ssl tdb
+    tokyocabinet zlib zstd
   } {
     if {[opt-val with-$opt] ne {}} {
       define want-$opt 1
@@ -588,13 +590,33 @@
 }
 
 ###############################################################################
+# GNU SASL
+if {[get-define want-gsasl] && [get-define want-sasl]} {
+  user-error "Cannot specify both --gsasl and --sasl"
+}
+if {[get-define want-gsasl]} {
+  if {[get-define want-pkgconf]} {
+    pkgconf true libgsasl
+    define USE_SASL_GNU
+    define-feature SASL
+  } else {
+    if {[check-inc-and-lib gsasl [opt-val with-gsasl $prefix] \
+                           gsasl.h gsasl_init gsasl]} {
+      define USE_SASL_GNU
+    } else {
+      user-error "Unable to find GNU SASL"
+    }
+  }
+}
+
+###############################################################################
 # SASL
 if {[get-define want-sasl]} {
   if {[get-define want-pkgconf]} {
     pkgconf true libsasl2
     # RHEL6 doesn't have this function yet
     cc-check-functions sasl_client_done
-    define USE_SASL
+    define USE_SASL_CYRUS
     define-feature SASL
   } else {
     foreach sasl_lib {sasl2 sasl} {
@@ -602,11 +624,11 @@
                              sasl/sasl.h sasl_encode64 $sasl_lib]} {
         # RHEL6 doesn't have this function yet
         cc-check-functions sasl_client_done
-        define USE_SASL
+        define USE_SASL_CYRUS
         break
       }
     }
-    if {![get-define USE_SASL]} {
+    if {![get-define USE_SASL_CYRUS]} {
       user-error "Unable to find SASL"
     }
   }
--- /dev/null
+++ b/conn/gsasl.c
@@ -0,0 +1,223 @@
+/**
+ * @file
+ * GNU SASL authentication support
+ *
+ * @authors
+ * Copyright (C) 2022 Richard Russon <rich@flatcap.org>
+ *
+ * @copyright
+ * This program is free software: you can redistribute it and/or modify it under
+ * the terms of the GNU General Public License as published by the Free Software
+ * Foundation, either version 2 of the License, or (at your option) any later
+ * version.
+ *
+ * This program is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along with
+ * this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+/**
+ * @page conn_gsasl GNU SASL authentication support
+ *
+ * GNU SASL authentication support
+ */
+
+#include "config.h"
+#include <gsasl.h>
+#include <stdbool.h>
+#include <string.h>
+#include "mutt/lib.h"
+#include "connaccount.h"
+#include "connection.h"
+#include "gsasl2.h"
+#include "mutt_account.h"
+
+static Gsasl *mutt_gsasl_ctx = NULL;
+
+/**
+ * mutt_gsasl_callback - Callback to retrieve authname or user from ConnAccount
+ * @param ctx  GNU SASL context
+ * @param sctx GNU SASL session
+ * @param prop Property to get, e.g. GSASL_PASSWORD
+ * @retval num GNU SASL error code, e.g. GSASL_OK
+ */
+static int mutt_gsasl_callback(Gsasl *ctx, Gsasl_session *sctx, Gsasl_property prop)
+{
+  int rc = GSASL_NO_CALLBACK;
+
+  struct Connection *conn = gsasl_session_hook_get(sctx);
+  if (!conn)
+  {
+    mutt_debug(LL_DEBUG1, "missing session hook data!\n");
+    return rc;
+  }
+
+  switch (prop)
+  {
+    case GSASL_PASSWORD:
+      if (mutt_account_getpass(&conn->account))
+        return rc;
+      gsasl_property_set(sctx, GSASL_PASSWORD, conn->account.pass);
+      rc = GSASL_OK;
+      break;
+
+    case GSASL_AUTHID:
+      /* whom the provided password belongs to: login */
+      if (mutt_account_getlogin(&conn->account))
+        return rc;
+      gsasl_property_set(sctx, GSASL_AUTHID, conn->account.login);
+      rc = GSASL_OK;
+      break;
+
+    case GSASL_AUTHZID:
+      /* name of the user whose mail/resources you intend to access: user */
+      if (mutt_account_getuser(&conn->account))
+        return rc;
+      gsasl_property_set(sctx, GSASL_AUTHZID, conn->account.user);
+      rc = GSASL_OK;
+      break;
+
+    case GSASL_ANONYMOUS_TOKEN:
+      gsasl_property_set(sctx, GSASL_ANONYMOUS_TOKEN, "dummy");
+      rc = GSASL_OK;
+      break;
+
+    case GSASL_SERVICE:
+    {
+      const char *service = NULL;
+      switch (conn->account.type)
+      {
+        case MUTT_ACCT_TYPE_IMAP:
+          service = "imap";
+          break;
+        case MUTT_ACCT_TYPE_POP:
+          service = "pop";
+          break;
+        case MUTT_ACCT_TYPE_SMTP:
+          service = "smtp";
+          break;
+        default:
+          return rc;
+      }
+      gsasl_property_set(sctx, GSASL_SERVICE, service);
+      rc = GSASL_OK;
+      break;
+    }
+
+    case GSASL_HOSTNAME:
+      gsasl_property_set(sctx, GSASL_HOSTNAME, conn->account.host);
+      rc = GSASL_OK;
+      break;
+
+    default:
+      break;
+  }
+
+  return rc;
+}
+
+/**
+ * mutt_gsasl_init - Initialise GNU SASL library
+ * @retval true Success
+ */
+static bool mutt_gsasl_init(void)
+{
+  if (mutt_gsasl_ctx)
+    return true;
+
+  int rc = gsasl_init(&mutt_gsasl_ctx);
+  if (rc != GSASL_OK)
+  {
+    mutt_gsasl_ctx = NULL;
+    mutt_debug(LL_DEBUG1, "libgsasl initialisation failed (%d): %s.\n", rc,
+               gsasl_strerror(rc));
+    return false;
+  }
+
+  gsasl_callback_set(mutt_gsasl_ctx, mutt_gsasl_callback);
+  return true;
+}
+
+/**
+ * mutt_gsasl_done - Shutdown GNU SASL library
+ */
+void mutt_gsasl_done(void)
+{
+  if (!mutt_gsasl_ctx)
+    return;
+
+  gsasl_done(mutt_gsasl_ctx);
+  mutt_gsasl_ctx = NULL;
+}
+
+/**
+ * mutt_gsasl_get_mech - Pick a connection mechanism
+ * @param requested_mech  Requested mechanism
+ * @param server_mechlist Server's list of mechanisms
+ * @retval ptr Selected mechanism string
+ */
+const char *mutt_gsasl_get_mech(const char *requested_mech, const char *server_mechlist)
+{
+  if (!mutt_gsasl_init())
+    return NULL;
+
+  /* libgsasl does not do case-independent string comparisons,
+   * and stores its methods internally in uppercase. */
+  char *uc_server_mechlist = mutt_str_dup(server_mechlist);
+  if (uc_server_mechlist)
+    mutt_str_upper(uc_server_mechlist);
+
+  char *uc_requested_mech = mutt_str_dup(requested_mech);
+  if (uc_requested_mech)
+    mutt_str_upper(uc_requested_mech);
+
+  const char *sel_mech = NULL;
+  if (uc_requested_mech)
+    sel_mech = gsasl_client_suggest_mechanism(mutt_gsasl_ctx, uc_requested_mech);
+  else
+    sel_mech = gsasl_client_suggest_mechanism(mutt_gsasl_ctx, uc_server_mechlist);
+
+  FREE(&uc_requested_mech);
+  FREE(&uc_server_mechlist);
+
+  return sel_mech;
+}
+
+/**
+ * mutt_gsasl_client_new - Create a new GNU SASL client
+ * @param conn Connection to a server
+ * @param mech Mechanisms to use
+ * @param sctx GNU SASL Session
+ * @retval  0 Success
+ * @retval -1 Error
+ */
+int mutt_gsasl_client_new(struct Connection *conn, const char *mech, Gsasl_session **sctx)
+{
+  if (!mutt_gsasl_init())
+    return -1;
+
+  int rc = gsasl_client_start(mutt_gsasl_ctx, mech, sctx);
+  if (rc != GSASL_OK)
+  {
+    *sctx = NULL;
+    mutt_debug(LL_DEBUG1, "gsasl_client_start failed (%d): %s.\n", rc, gsasl_strerror(rc));
+    return -1;
+  }
+
+  gsasl_session_hook_set(*sctx, conn);
+  return 0;
+}
+
+/**
+ * mutt_gsasl_client_finish - Free a GNU SASL client
+ * @param sctx GNU SASL Session
+ */
+void mutt_gsasl_client_finish(Gsasl_session **sctx)
+{
+  gsasl_finish(*sctx);
+  *sctx = NULL;
+}
--- /dev/null
+++ b/conn/gsasl2.h
@@ -0,0 +1,35 @@
+/**
+ * @file
+ * GNU SASL authentication support
+ *
+ * @authors
+ * Copyright (C) 2022 Richard Russon <rich@flatcap.org>
+ *
+ * @copyright
+ * This program is free software: you can redistribute it and/or modify it under
+ * the terms of the GNU General Public License as published by the Free Software
+ * Foundation, either version 2 of the License, or (at your option) any later
+ * version.
+ *
+ * This program is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along with
+ * this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#ifndef MUTT_CONN_GSASL_H
+#define MUTT_CONN_GSASL_H
+
+#include <gsasl.h>
+
+struct Connection;
+
+void        mutt_gsasl_client_finish(Gsasl_session **sctx);
+int         mutt_gsasl_client_new   (struct Connection *conn, const char *mech, Gsasl_session **sctx);
+void        mutt_gsasl_done         (void);
+const char *mutt_gsasl_get_mech     (const char *requested_mech, const char *server_mechlist);
+
+#endif /* MUTT_CONN_GSASL_H */
--- a/conn/lib.h
+++ b/conn/lib.h
@@ -31,6 +31,7 @@
  * | conn/connaccount.c  | @subpage conn_account        |
  * | conn/getdomain.c    | @subpage conn_getdomain      |
  * | conn/gnutls.c       | @subpage conn_gnutls         |
+ * | conn/gsasl.c        | @subpage conn_gsasl          |
  * | conn/gui.c          | @subpage conn_dlg_verifycert |
  * | conn/openssl.c      | @subpage conn_openssl        |
  * | conn/raw.c          | @subpage conn_raw            |
@@ -50,7 +51,10 @@
 #include "connection.h"
 #include "sasl_plain.h"
 #include "socket.h"
-#ifdef USE_SASL
+#ifdef USE_SASL_GNU
+#include "gsasl2.h"
+#endif
+#ifdef USE_SASL_CYRUS
 #include "sasl.h"
 #endif
 #ifdef USE_ZLIB
--- a/conn/sasl.h
+++ b/conn/sasl.h
@@ -20,8 +20,6 @@
  * this program.  If not, see <http://www.gnu.org/licenses/>.
  */
 
-/* common SASL helper routines */
-
 #ifndef MUTT_CONN_SASL_H
 #define MUTT_CONN_SASL_H
 
--- a/imap/auth.c
+++ b/imap/auth.c
@@ -61,8 +61,10 @@
   { imap_auth_oauth, "oauthbearer" },
   { imap_auth_xoauth2, "xoauth2" },
   { imap_auth_plain, "plain" },
-#ifdef USE_SASL
+#if defined(USE_SASL_CYRUS)
   { imap_auth_sasl, NULL },
+#elif defined(USE_SASL_GNU)
+  { imap_auth_gsasl, NULL },
 #else
   { imap_auth_anon, "anonymous" },
 #endif
@@ -70,7 +72,7 @@
   { imap_auth_gss, "gssapi" },
 #endif
 /* SASL includes CRAM-MD5 (and GSSAPI, but that's not enabled by default) */
-#ifndef USE_SASL
+#ifndef HAVE_SASL
   { imap_auth_cram_md5, "cram-md5" },
 #endif
   { imap_auth_login, "login" },
--- a/imap/auth.h
+++ b/imap/auth.h
@@ -45,7 +45,7 @@
 
 /* external authenticator prototypes */
 enum ImapAuthRes imap_auth_plain(struct ImapAccountData *adata, const char *method);
-#ifndef USE_SASL
+#ifndef USE_SASL_CYRUS
 enum ImapAuthRes imap_auth_anon(struct ImapAccountData *adata, const char *method);
 enum ImapAuthRes imap_auth_cram_md5(struct ImapAccountData *adata, const char *method);
 #endif
@@ -53,9 +53,12 @@
 #ifdef USE_GSS
 enum ImapAuthRes imap_auth_gss(struct ImapAccountData *adata, const char *method);
 #endif
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
 enum ImapAuthRes imap_auth_sasl(struct ImapAccountData *adata, const char *method);
 #endif
+#ifdef USE_SASL_GNU
+enum ImapAuthRes imap_auth_gsasl(struct ImapAccountData *adata, const char *method);
+#endif
 enum ImapAuthRes imap_auth_oauth(struct ImapAccountData *adata, const char *method);
 enum ImapAuthRes imap_auth_xoauth2(struct ImapAccountData *adata, const char *method);
 
--- /dev/null
+++ b/imap/auth_gsasl.c
@@ -0,0 +1,152 @@
+/**
+ * @file
+ * IMAP GNU SASL authentication method
+ *
+ * @authors
+ * Copyright (C) 2022 Richard Russon <rich@flatcap.org>
+ *
+ * @copyright
+ * This program is free software: you can redistribute it and/or modify it under
+ * the terms of the GNU General Public License as published by the Free Software
+ * Foundation, either version 2 of the License, or (at your option) any later
+ * version.
+ *
+ * This program is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along with
+ * this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+/**
+ * @page imap_auth_gsasl GNU SASL authentication
+ *
+ * IMAP GNU SASL authentication method
+ */
+
+#include "config.h"
+#include <stddef.h>
+#include <gsasl.h>
+#include "private.h"
+#include "mutt/lib.h"
+#include "conn/lib.h"
+#include "adata.h"
+#include "auth.h"
+#include "mutt_socket.h"
+
+/**
+ * imap_auth_gsasl - GNU SASL authenticator - Implements ImapAuth::authenticate()
+ */
+enum ImapAuthRes imap_auth_gsasl(struct ImapAccountData *adata, const char *method)
+{
+  Gsasl_session *gsasl_session = NULL;
+  struct Buffer *output_buf = NULL;
+  char *imap_step_output = NULL;
+  int rc = IMAP_AUTH_FAILURE;
+  int gsasl_rc = GSASL_OK;
+  int imap_step_rc = IMAP_RES_CONTINUE;
+
+  const char *chosen_mech = mutt_gsasl_get_mech(method, adata->capstr);
+  if (!chosen_mech)
+  {
+    mutt_debug(LL_DEBUG2, "mutt_gsasl_get_mech() returned no usable mech\n");
+    return IMAP_AUTH_UNAVAIL;
+  }
+
+  mutt_debug(LL_DEBUG2, "using mech %s\n", chosen_mech);
+
+  if (mutt_gsasl_client_new(adata->conn, chosen_mech, &gsasl_session) < 0)
+  {
+    mutt_debug(LL_DEBUG1, "Error allocating GSASL connection.\n");
+    return IMAP_AUTH_UNAVAIL;
+  }
+
+  mutt_message(_("Authenticating (%s)..."), chosen_mech);
+
+  output_buf = mutt_buffer_pool_get();
+  mutt_buffer_printf(output_buf, "AUTHENTICATE %s", chosen_mech);
+  if (adata->capabilities & IMAP_CAP_SASL_IR)
+  {
+    char *gsasl_step_output = NULL;
+    gsasl_rc = gsasl_step64(gsasl_session, "", &gsasl_step_output);
+    if ((gsasl_rc != GSASL_NEEDS_MORE) && (gsasl_rc != GSASL_OK))
+    {
+      mutt_debug(LL_DEBUG1, "gsasl_step64() failed (%d): %s\n", gsasl_rc,
+                 gsasl_strerror(gsasl_rc));
+      rc = IMAP_AUTH_UNAVAIL;
+      goto bail;
+    }
+
+    mutt_buffer_addch(output_buf, ' ');
+    mutt_buffer_addstr(output_buf, gsasl_step_output);
+    gsasl_free(gsasl_step_output);
+  }
+  imap_cmd_start(adata, mutt_buffer_string(output_buf));
+
+  do
+  {
+    do
+    {
+      imap_step_rc = imap_cmd_step(adata);
+    } while (imap_step_rc == IMAP_RES_CONTINUE);
+
+    if ((imap_step_rc == IMAP_RES_BAD) || (imap_step_rc == IMAP_RES_NO))
+      goto bail;
+
+    if (imap_step_rc != IMAP_RES_RESPOND)
+      break;
+
+    imap_step_output = imap_next_word(adata->buf);
+
+    char *gsasl_step_output = NULL;
+    gsasl_rc = gsasl_step64(gsasl_session, imap_step_output, &gsasl_step_output);
+    if ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK))
+    {
+      mutt_buffer_strcpy(output_buf, gsasl_step_output);
+      gsasl_free(gsasl_step_output);
+    }
+    else
+    {
+      // sasl error occured, send an abort string
+      mutt_debug(LL_DEBUG1, "gsasl_step64() failed (%d): %s\n", gsasl_rc,
+                 gsasl_strerror(gsasl_rc));
+      mutt_buffer_strcpy(output_buf, "*");
+    }
+
+    mutt_buffer_addstr(output_buf, "\r\n");
+    mutt_socket_send(adata->conn, mutt_buffer_string(output_buf));
+  } while ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK));
+
+  if (imap_step_rc != IMAP_RES_OK)
+  {
+    do
+      imap_step_rc = imap_cmd_step(adata);
+    while (imap_step_rc == IMAP_RES_CONTINUE);
+  }
+
+  if (imap_step_rc == IMAP_RES_RESPOND)
+  {
+    mutt_socket_send(adata->conn, "*\r\n");
+    goto bail;
+  }
+
+  if ((gsasl_rc != GSASL_OK) || (imap_step_rc != IMAP_RES_OK))
+    goto bail;
+
+  if (imap_code(adata->buf))
+    rc = IMAP_AUTH_SUCCESS;
+
+bail:
+  mutt_buffer_pool_release(&output_buf);
+  mutt_gsasl_client_finish(&gsasl_session);
+
+  if (rc == IMAP_AUTH_FAILURE)
+  {
+    mutt_debug(LL_DEBUG2, "%s failed\n", chosen_mech);
+    mutt_error(_("SASL authentication failed"));
+  }
+
+  return rc;
+}
--- a/imap/auth_sasl.c
+++ b/imap/auth_sasl.c
@@ -41,7 +41,7 @@
 #include "mutt_socket.h"
 
 /**
- * imap_auth_sasl - Default authenticator if available - Implements ImapAuth::authenticate()
+ * imap_auth_sasl - SASL authenticator - Implements ImapAuth::authenticate()
  */
 enum ImapAuthRes imap_auth_sasl(struct ImapAccountData *adata, const char *method)
 {
--- a/imap/config.c
+++ b/imap/config.c
@@ -32,8 +32,10 @@
 #include <stdbool.h>
 #include <stdint.h>
 #include "mutt/lib.h"
-#include "conn/lib.h"
 #include "auth.h"
+#ifdef USE_SASL_CYRUS
+#include "conn/lib.h"
+#endif
 
 /**
  * imap_auth_validator - Validate the "imap_authenticators" config variable - Implements ConfigDef::validator() - @ingroup cfg_def_validator
@@ -50,7 +52,7 @@
   {
     if (imap_auth_is_valid(np->data))
       continue;
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
     if (sasl_auth_validator(np->data))
       continue;
 #endif
--- a/main.c
+++ b/main.c
@@ -1383,9 +1383,12 @@
 #ifdef USE_IMAP
     imap_logout_all();
 #endif
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
     mutt_sasl_done();
 #endif
+#ifdef USE_SASL_GNU
+    mutt_gsasl_done();
+#endif
 #ifdef USE_AUTOCRYPT
     mutt_autocrypt_cleanup();
 #endif
--- a/pop/auth.c
+++ b/pop/auth.c
@@ -39,12 +39,108 @@
 #include "conn/lib.h"
 #include "adata.h"
 #include "mutt_socket.h"
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
 #include <sasl/sasl.h>
 #include <sasl/saslutil.h>
 #endif
+#ifdef USE_SASL_GNU
+#include <gsasl.h>
+#endif
+
+#ifdef USE_SASL_GNU
+/**
+ * pop_auth_gsasl - POP GNU SASL authenticator - Implements PopAuth::authenticate()
+ */
+static enum PopAuthRes pop_auth_gsasl(struct PopAccountData *adata, const char *method)
+{
+  Gsasl_session *gsasl_session = NULL;
+  struct Buffer *output_buf = NULL;
+  struct Buffer *input_buf = NULL;
+  int rc = POP_A_FAILURE;
+  int gsasl_rc = GSASL_OK;
+
+  const char *chosen_mech = mutt_gsasl_get_mech(method,
+                                                mutt_buffer_string(&adata->auth_list));
+  if (!chosen_mech)
+  {
+    mutt_debug(LL_DEBUG2, "returned no usable mech\n");
+    return POP_A_UNAVAIL;
+  }
+
+  mutt_debug(LL_DEBUG2, "using mech %s\n", chosen_mech);
+
+  if (mutt_gsasl_client_new(adata->conn, chosen_mech, &gsasl_session) < 0)
+  {
+    mutt_debug(LL_DEBUG1, "Error allocating GSASL connection.\n");
+    return POP_A_UNAVAIL;
+  }
+
+  mutt_message(_("Authenticating (%s)..."), chosen_mech);
+
+  output_buf = mutt_buffer_pool_get();
+  input_buf = mutt_buffer_pool_get();
+  mutt_buffer_printf(output_buf, "AUTH %s\r\n", chosen_mech);
+
+  do
+  {
+    if (mutt_socket_send(adata->conn, mutt_buffer_string(output_buf)) < 0)
+    {
+      adata->status = POP_DISCONNECTED;
+      rc = POP_A_SOCKET;
+      goto fail;
+    }
+
+    if (mutt_socket_buffer_readln(input_buf, adata->conn) < 0)
+    {
+      adata->status = POP_DISCONNECTED;
+      rc = POP_A_SOCKET;
+      goto fail;
+    }
+
+    if (!mutt_strn_equal(mutt_buffer_string(input_buf), "+ ", 2))
+      break;
+
+    const char *pop_auth_data = mutt_buffer_string(input_buf) + 2;
+    char *gsasl_step_output = NULL;
+    gsasl_rc = gsasl_step64(gsasl_session, pop_auth_data, &gsasl_step_output);
+    if ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK))
+    {
+      mutt_buffer_strcpy(output_buf, gsasl_step_output);
+      mutt_buffer_addstr(output_buf, "\r\n");
+      gsasl_free(gsasl_step_output);
+    }
+    else
+    {
+      mutt_debug(LL_DEBUG1, "gsasl_step64() failed (%d): %s\n", gsasl_rc,
+                 gsasl_strerror(gsasl_rc));
+    }
+  } while ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK));
+
+  if (mutt_strn_equal(mutt_buffer_string(input_buf), "+ ", 2))
+  {
+    mutt_socket_send(adata->conn, "*\r\n");
+    goto fail;
+  }
+
+  if (mutt_strn_equal(mutt_buffer_string(input_buf), "+OK", 3) && (gsasl_rc == GSASL_OK))
+    rc = POP_A_SUCCESS;
 
-#ifdef USE_SASL
+fail:
+  mutt_buffer_pool_release(&input_buf);
+  mutt_buffer_pool_release(&output_buf);
+  mutt_gsasl_client_finish(&gsasl_session);
+
+  if (rc == POP_A_FAILURE)
+  {
+    mutt_debug(LL_DEBUG2, "%s failed\n", chosen_mech);
+    mutt_error(_("SASL authentication failed."));
+  }
+
+  return rc;
+}
+#endif
+
+#ifdef USE_SASL_CYRUS
 /**
  * pop_auth_sasl - POP SASL authenticator - Implements PopAuth::authenticate()
  */
@@ -384,9 +480,12 @@
 static const struct PopAuth PopAuthenticators[] = {
   // clang-format off
   { pop_auth_oauth, "oauthbearer" },
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
   { pop_auth_sasl, NULL },
 #endif
+#ifdef USE_SASL_GNU
+  { pop_auth_gsasl, NULL },
+#endif
   { pop_auth_apop, "apop" },
   { pop_auth_user, "user" },
   { NULL, NULL },
--- a/pop/config.c
+++ b/pop/config.c
@@ -33,7 +33,6 @@
 #include <stdint.h>
 #include "private.h"
 #include "mutt/lib.h"
-#include "conn/lib.h"
 
 /**
  * pop_auth_validator - Validate the "pop_authenticators" config variable - Implements ConfigDef::validator() - @ingroup cfg_def_validator
@@ -50,7 +49,7 @@
   {
     if (pop_auth_is_valid(np->data))
       continue;
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
     if (sasl_auth_validator(np->data))
       continue;
 #endif
--- a/send/config.c
+++ b/send/config.c
@@ -32,8 +32,10 @@
 #include <stdbool.h>
 #include <stdint.h>
 #include "mutt/lib.h"
-#include "conn/lib.h"
 #include "lib.h"
+#ifdef USE_SASL_CYRUS
+#include "conn/lib.h"
+#endif
 
 /**
  * wrapheaders_validator - Validate the "wrap_headers" config variable - Implements ConfigDef::validator() - @ingroup cfg_def_validator
@@ -68,7 +70,7 @@
   {
     if (smtp_auth_is_valid(np->data))
       continue;
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
     if (sasl_auth_validator(np->data))
       continue;
 #endif
--- a/send/smtp.c
+++ b/send/smtp.c
@@ -50,7 +50,11 @@
 #include "mutt_account.h"
 #include "mutt_globals.h"
 #include "mutt_socket.h"
-#ifdef USE_SASL
+#ifdef USE_SASL_GNU
+#include <gsasl.h>
+#include "options.h"
+#endif
+#ifdef USE_SASL_CYRUS
 #include <sasl/sasl.h>
 #include <sasl/saslutil.h>
 #include "options.h"
@@ -419,7 +423,184 @@
   return smtp_get_resp(adata);
 }
 
-#ifdef USE_SASL
+#ifdef USE_SASL_GNU
+/**
+ * smtp_code - Extract an SMTP return code from a string
+ * @param[in]  str String to parse
+ * @param[in]  len Length of string
+ * @param[out] n   SMTP return code result
+ * @retval true Success
+ *
+ * Note: the 'len' parameter is actually the number of bytes, as
+ * returned by mutt_socket_readln().  If all callers are converted to
+ * mutt_socket_buffer_readln() we can pass in the actual len, or
+ * perhaps the buffer itself.
+ */
+static int smtp_code(const char *str, size_t len, int *n)
+{
+  char code[4];
+
+  if (len < 4)
+    return false;
+  code[0] = str[0];
+  code[1] = str[1];
+  code[2] = str[2];
+  code[3] = 0;
+
+  const char *end = mutt_str_atoi(code, n);
+  if (!end || (*end != '\0'))
+    return false;
+  return true;
+}
+
+/**
+ * smtp_get_auth_response - Get the SMTP authorisation response
+ * @param[in]  conn         Connection to a server
+ * @param[in]  input_buf    Temp buffer for strings returned by the server
+ * @param[out] smtp_rc      SMTP return code result
+ * @param[out] response_buf Text after the SMTP response code
+ * @retval  0 Success
+ * @retval -1 Error
+ *
+ * Did the SMTP authorisation succeed?
+ */
+static int smtp_get_auth_response(struct Connection *conn, struct Buffer *input_buf,
+                                  int *smtp_rc, struct Buffer *response_buf)
+{
+  mutt_buffer_reset(response_buf);
+  do
+  {
+    if (mutt_socket_buffer_readln(input_buf, conn) < 0)
+      return -1;
+    if (!smtp_code(mutt_buffer_string(input_buf),
+                   mutt_buffer_len(input_buf) + 1 /* number of bytes */, smtp_rc))
+    {
+      return -1;
+    }
+
+    if (*smtp_rc != SMTP_READY)
+      break;
+
+    const char *smtp_response = mutt_buffer_string(input_buf) + 3;
+    if (*smtp_response)
+    {
+      smtp_response++;
+      mutt_buffer_addstr(response_buf, smtp_response);
+    }
+  } while (mutt_buffer_string(input_buf)[3] == '-');
+
+  return 0;
+}
+
+/**
+ * smtp_auth_gsasl - Authenticate using SASL
+ * @param adata    SMTP Account data
+ * @param mechlist List of mechanisms to use
+ * @retval  0 Success
+ * @retval <0 Error, e.g. #SMTP_AUTH_FAIL
+ */
+static int smtp_auth_gsasl(struct SmtpAccountData *adata, const char *mechlist)
+{
+  Gsasl_session *gsasl_session = NULL;
+  struct Buffer *input_buf = NULL, *output_buf = NULL, *smtp_response_buf = NULL;
+  int rc = SMTP_AUTH_FAIL, gsasl_rc = GSASL_OK, smtp_rc;
+
+  const char *chosen_mech = mutt_gsasl_get_mech(mechlist, adata->auth_mechs);
+  if (!chosen_mech)
+  {
+    mutt_debug(LL_DEBUG2, "returned no usable mech\n");
+    return SMTP_AUTH_UNAVAIL;
+  }
+
+  mutt_debug(LL_DEBUG2, "using mech %s\n", chosen_mech);
+
+  if (mutt_gsasl_client_new(adata->conn, chosen_mech, &gsasl_session) < 0)
+  {
+    mutt_debug(LL_DEBUG1, "Error allocating GSASL connection.\n");
+    return SMTP_AUTH_UNAVAIL;
+  }
+
+  if (!OptNoCurses)
+    mutt_message(_("Authenticating (%s)..."), chosen_mech);
+
+  input_buf = mutt_buffer_pool_get();
+  output_buf = mutt_buffer_pool_get();
+  smtp_response_buf = mutt_buffer_pool_get();
+
+  mutt_buffer_printf(output_buf, "AUTH %s", chosen_mech);
+
+  /* Work around broken SMTP servers. See Debian #1010658.
+   * The msmtp source also forces IR for PLAIN because the author
+   * encountered difficulties with a server requiring it. */
+  if (mutt_str_equal(chosen_mech, "PLAIN"))
+  {
+    char *gsasl_step_output = NULL;
+    gsasl_rc = gsasl_step64(gsasl_session, "", &gsasl_step_output);
+    if (gsasl_rc != GSASL_NEEDS_MORE && gsasl_rc != GSASL_OK)
+    {
+      mutt_debug(LL_DEBUG1, "gsasl_step64() failed (%d): %s\n", gsasl_rc,
+                 gsasl_strerror(gsasl_rc));
+      goto fail;
+    }
+
+    mutt_buffer_addch(output_buf, ' ');
+    mutt_buffer_addstr(output_buf, gsasl_step_output);
+    gsasl_free(gsasl_step_output);
+  }
+
+  mutt_buffer_addstr(output_buf, "\r\n");
+
+  do
+  {
+    if (mutt_socket_send(adata->conn, mutt_buffer_string(output_buf)) < 0)
+      goto fail;
+
+    if (smtp_get_auth_response(adata->conn, input_buf, &smtp_rc, smtp_response_buf) < 0)
+      goto fail;
+
+    if (smtp_rc != SMTP_READY)
+      break;
+
+    char *gsasl_step_output = NULL;
+    gsasl_rc = gsasl_step64(gsasl_session, mutt_buffer_string(smtp_response_buf),
+                            &gsasl_step_output);
+    if ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK))
+    {
+      mutt_buffer_strcpy(output_buf, gsasl_step_output);
+      mutt_buffer_addstr(output_buf, "\r\n");
+      gsasl_free(gsasl_step_output);
+    }
+    else
+    {
+      mutt_debug(LL_DEBUG1, "gsasl_step64() failed (%d): %s\n", gsasl_rc,
+                 gsasl_strerror(gsasl_rc));
+    }
+  } while ((gsasl_rc == GSASL_NEEDS_MORE) || (gsasl_rc == GSASL_OK));
+
+  if (smtp_rc == SMTP_READY)
+  {
+    mutt_socket_send(adata->conn, "*\r\n");
+    goto fail;
+  }
+
+  if (smtp_success(smtp_rc) && (gsasl_rc == GSASL_OK))
+    rc = SMTP_AUTH_SUCCESS;
+
+fail:
+  mutt_buffer_pool_release(&input_buf);
+  mutt_buffer_pool_release(&output_buf);
+  mutt_buffer_pool_release(&smtp_response_buf);
+  mutt_gsasl_client_finish(&gsasl_session);
+
+  if (rc == SMTP_AUTH_FAIL)
+    mutt_debug(LL_DEBUG2, "%s failed\n", chosen_mech);
+
+  return rc;
+}
+#endif
+
+
+#ifdef USE_SASL_CYRUS
 /**
  * smtp_auth_sasl - Authenticate using SASL
  * @param adata    SMTP Account data
@@ -721,9 +902,12 @@
   { smtp_auth_xoauth2, "xoauth2" },
   { smtp_auth_plain, "plain" },
   { smtp_auth_login, "login" },
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
   { smtp_auth_sasl, NULL },
 #endif
+#ifdef USE_SASL_GNU
+  { smtp_auth_gsasl, NULL },
+#endif
   // clang-format on
 };
 
@@ -785,8 +969,10 @@
     /* Fall back to default: any authenticator */
     mutt_debug(LL_DEBUG2, "Falling back to smtp_auth_sasl, if using sasl.\n");
 
-#ifdef USE_SASL
+#if defined(USE_SASL_CYRUS)
     r = smtp_auth_sasl(adata, adata->auth_mechs);
+#elif defined(USE_SASL_GNU)
+    r = smtp_auth_gsasl(adata, adata->auth_mechs);
 #else
     mutt_error(_("SMTP authentication requires SASL"));
     r = SMTP_AUTH_UNAVAIL;
--- a/version.c
+++ b/version.c
@@ -198,6 +198,11 @@
 #else
   { "gpgme", 0 },
 #endif
+#ifdef USE_SASL_GNU
+  { "gsasl", 1 },
+#else
+  { "gsasl", 0 },
+#endif
 #ifdef USE_GSS
   { "gss", 1 },
 #else
@@ -264,7 +269,7 @@
 #ifndef HAVE_PCRE2
   { "regex", 1 },
 #endif
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
   { "sasl", 1 },
 #else
   { "sasl", 0 },
--- a/nntp/nntp.c
+++ b/nntp/nntp.c
@@ -65,7 +65,7 @@
 #ifdef USE_HCACHE
 #include "protos.h"
 #endif
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
 #include <sasl/sasl.h>
 #include <sasl/saslutil.h>
 #endif
@@ -186,7 +186,7 @@
       mutt_str_cat(buf, sizeof(buf), " ");
       mutt_str_copy(authinfo, buf + plen - 1, sizeof(authinfo));
     }
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
     else if ((plen = mutt_str_startswith(buf, "SASL ")))
     {
       char *p = buf + plen;
@@ -209,7 +209,7 @@
     }
   } while (!mutt_str_equal(".", buf));
   *buf = '\0';
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
   if (adata->authenticators && mutt_istr_find(authinfo, " SASL "))
     mutt_str_copy(buf, adata->authenticators, sizeof(buf));
 #endif
@@ -374,7 +374,7 @@
   return 0;
 }
 
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
 /**
  * nntp_memchr - Look for a char in a binary buf, conveniently
  * @param haystack [in/out] input: start here, output: store address of hit
@@ -532,7 +532,7 @@
       }
       else
       {
-#ifdef USE_SASL
+#ifdef USE_SASL_CYRUS
         sasl_conn_t *saslconn = NULL;
         sasl_interact_t *interaction = NULL;
         int rc;
@@ -654,7 +654,7 @@
           continue;
 #else
         continue;
-#endif /* USE_SASL */
+#endif /* USE_SASL_CYRUS */
       }
 
       // L10N: %s is the method name, e.g. Anonymous, CRAM-MD5, GSSAPI, SASL
